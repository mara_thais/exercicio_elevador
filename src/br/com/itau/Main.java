package br.com.itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int pAtual = 0;
        int pTotal;
        int capacidade;
        int aux;

        Scanner in = new Scanner(System.in);

        // Elevador
        Elevador elevador[]=new Elevador[1];
        elevador[0]=new Elevador(0,0,0,0);

        //Inicializando (capacidade total e andares)
        System.out.printf("Qual a capacidade total de pessoas no elevador? Escreva Aqui: ");
        capacidade=in.nextInt();
        System.out.printf("Quantos andares tem o prédio? Escreva Aqui: ");
        pTotal=in.nextInt();
        elevador[0].inicializar(capacidade,pTotal);

        while(true){
            mostrarOpcoes();
            pAtual = in.nextInt();
            if(pAtual==0)
                break;

            System.out.printf("\n\n");
            switch (pAtual) {
                case 1:
                    System.out.printf("Setar Número de pessoas atualmente: ");
                    aux=in.nextInt();
                    if(aux<elevador[0].getCapacidade())
                        elevador[0].setPessoasTotal(aux);
                    break;
                case 2:
                    //só sobe se o andar atual for menor que o total
                    if (elevador[0].getAndarAtual()<elevador[0].getAndarTotal())
                        elevador[0].subirAndar();
                    break;
                case 3:
                    //só desce se o andar atual for maior que o terreo
                    if (elevador[0].getAndarAtual()>0)
                        elevador[0].descerAndar();
                    break;
                case 4:
                    //só entra, se não tiver atingido a capacidade
                    if (elevador[0].getPessoasTotal()<elevador[0].getCapacidade())
                        elevador[0].entrar();
                    break;
                case 5:
                    //só sai, se houverem pessoas (>0)
                    if (elevador[0].getPessoasTotal()>0)
                        elevador[0].sair();
                    break;
                case 6:
                    System.out.printf("Quantidade de Pessoas: %d\n",elevador[0].getPessoasTotal());
                    System.out.printf("Capacidade Suportada: %d\n",elevador[0].getCapacidade());
                    System.out.printf("Andar Atual: %d\n",elevador[0].getAndarAtual());
                    System.out.printf("Quantidade de Andares: %d\n",elevador[0].getAndarTotal());
                    //espera 5 segundos
                    try { Thread.sleep (5000); }
                    catch (InterruptedException ex) {}
                    break;
                default:
                    System.out.printf("\n --> Valor Inválido <--");
                    break;
            }
        }
        System.out.printf("\n\nPrograma Finalizado!\n\n");
    }

        //Mostra Opções
        private static void mostrarOpcoes(){
            System.out.printf("\n\n > Opções ");
            System.out.printf("\n  1- Número de pessoas Atual ");
            System.out.printf("\n  2- Subir Andar ");
            System.out.printf("\n  3- Descer Andar ");
            System.out.printf("\n  4- Entrar Pessoa ");
            System.out.printf("\n  5- Sair Pessoa ");
            System.out.printf("\n  6- Mostrar dados ");
            System.out.printf("\n  0- Sair ");
            System.out.printf("\n > Resposta: ");
        }
    }
