package br.com.itau;

import java.util.List;

public class Elevador {
    private int andarAtual;
    private int andarTotal;
    private int capacidade;
    private int pessoasTotal;

    public Elevador(int andarAtual, int andarTotal, int capacidade, int pessoasTotal) {
        this.andarAtual = andarAtual;
        this.andarTotal = andarTotal;
        this.capacidade = capacidade;
        this.pessoasTotal = pessoasTotal;
    }

    public int getAndarAtual() {
        return this.andarAtual;
    }

    public int getAndarTotal() {
        return this.andarTotal;
    }

    public int getCapacidade() {
        return this.capacidade;
    }

    public int getPessoasTotal() {
        return pessoasTotal;
    }

    public void setPessoasTotal(int nP) {
        this.pessoasTotal = nP;
    }

    //Inicializar
    public void inicializar(int capacidade,int andarTotal){
        this.andarTotal   = andarTotal;
        this.capacidade   = capacidade;
    }

    //Entrar
    public void entrar(){
        this.pessoasTotal+=1;
    }

    //Sair
    public void sair(){
        this.pessoasTotal-=1;

    }
    //Subir
    public void subirAndar(){
        this.andarAtual+=1;
    }

    //Descer
    public void descerAndar(){
        this.andarAtual-=1;
    }
}
